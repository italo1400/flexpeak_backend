<?php

namespace App\Http\Controllers;
use DB;
use App\Contato; 
use Illuminate\Database\QueryException;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as Controler;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class ControllerContato extends Controller
{
    public function getContato($id){
        $contato = Contato::where('id',$id)->get();
        return response()->json($contato);
    }

    public function listarContatos(){
        $contatos = Contato::get();
        return response()->json($contatos);
    }
    
    public function deletarContato($id){
        $res = Contato::where('id',$id)->delete();
        return response()->json($res);
    }

    public function inserirContato(Request $req){
        $res = Contato::insert(['nm_contato' => isset($req['nm_contato']) ? $req['nm_contato'] : 'Sem Nome',
                                'nr_telefone' => isset($req['nr_telefone']) ? $req['nr_telefone'] : 'Sem Telefone',
                                'ds_observacao' => isset($req['ds_observacao']) ? $req['ds_observacao'] : '',
                                'ds_email' => isset($req['ds_email']) ? $req['ds_email'] : 'Sem Email'
                                ]);
        return response()->json($res);
    }

    public function editarContato(Request $req){
            $res = Contato::where('id',$req['id'])
                        ->update(['nm_contato' => isset($req['nm_contato']) ? $req['nm_contato'] : 'Sem Nome',
                                    'nr_telefone' => isset($req['nr_telefone']) ? $req['nr_telefone'] : 'Sem Telefone',
                                    'ds_observacao' => isset($req['ds_observacao']) ? $req['ds_observacao'] : '',
                                    'ds_email' => isset($req['ds_email']) ? $req['ds_email'] : 'Sem Email'
                                    ]);           
                                                            
            return response()->json($res);         

    }
}
