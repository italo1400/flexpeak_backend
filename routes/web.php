<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/contato/{id}', 'ControllerContato@getContato');
Route::get('/contatos', 'ControllerContato@listarContatos');
Route::delete('/contato/{id}', 'ControllerContato@deletarContato');
Route::post('/contato', 'ControllerContato@inserirContato');
Route::post('/contato/editar', 'ControllerContato@editarContato');