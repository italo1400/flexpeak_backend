<?php

use Illuminate\Database\Seeder;

class contatos_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contatos = [
            [
                'nm_contato' => 'Somente teste',
                'nr_telefone' => '00000000',
                'ds_email' => 'teste@teste.com',
                'ds_observacao' => 'somente teste'
            ],
            [
                'nm_contato' => 'Somente teste',
                'nr_telefone' => '00000000',
                'ds_email' => 'teste@teste.com',
                'ds_observacao' => 'somente teste'
            ]
        ];
        DB::table('contatos')->insert($contatos);
    }
}
